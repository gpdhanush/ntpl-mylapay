# Mylapay Payment SDK

[![npm](https://img.shields.io/npm/v/razorpay.svg?maxAge=2592000?style=flat-square)](https://www.npmjs.com/package/mylapay-test)

<p align="center">
  <a href="https://www.mylapay.com">
    <img src="https://www.mylapay.com/assets/images/logo_latest.png" alt="Mylapay logo" width="200" height="100">
  </a>
</p>

Read up here for getting started and understanding the payment flow with Mylapay: <https://www.mylapay.com>

## Installation

```bash
npm i mylapay-test --save
```

## Documentation

Documentation of Mylapay's API and their usage is available at <https://www.mylapay.com>

### Basic Usage

Instantiate the Mylapay instance with `key` & `secret`. You can obtain the keys from the dashboard app ([https://dashboard.mylapay.com/#/app/keys](https://dashboard.mylapay.com/#/app/keys))

```js
const Mylapay = require('mylapay-test');

var instance = new Mylapay({
  key: 'YOUR_KEY_ID',
  secret: 'YOUR_KEY_SECRET',
  MerchantTxnID: string,
  amount: number,
  username: string,
  email: string,
  phone: string,
  surl: string,
  furl: string
});
```